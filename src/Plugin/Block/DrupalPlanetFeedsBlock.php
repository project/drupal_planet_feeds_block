<?php

namespace Drupal\drupal_planet_feeds_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Drupal Planet Feeds Block' block.
 *
 * @Block(
 *  id = "drupal_planet_feeds_block",
 *  admin_label = @Translation("Planet Drupal"),
 * )
 */
class DrupalPlanetFeedsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['number_of_feeds_to_display'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of feeds to display'),
      '#description' => $this->t('Enter the number of planets feeds to be displayed, 1 to 30 feeds are allowed.'),
      '#default_value' => $this->configuration['number_of_feeds_to_display'] ?? 1,
      '#min' => 1,
      '#max' => 30,
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['number_of_feeds_to_display'] = $form_state->getValue('number_of_feeds_to_display');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $number_of_feeds_to_display = $this->configuration['number_of_feeds_to_display'];
    if (isset($number_of_feeds_to_display)) {
      $build = [];
      $planet_links = [];
      $xml = simplexml_load_file('https://www.drupal.org/planet/rss.xml');
      $i = 0;
      foreach ($xml->channel->item as $item) {
        $i++;
        if ($i > $number_of_feeds_to_display) {
          continue;
        }
        $planet_data = (array) $item;
        $planet_links[$i] = [
          'link' => $planet_data['link'],
          'title' => $planet_data['title'],
        ];
      }
      $build[] = [
        '#theme' => 'drupal_planet_feeds_block',
        '#planet_links' => $planet_links,
        '#more_link' => 'https://www.drupal.org/planet',
      ];
      $build['#attached']['library'][] = 'drupal_planet_feeds_block/drupal_planet_feeds_block';
      $build['#cache']['max-age'] = 0;
    }
    return $build;
  }

}
